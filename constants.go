package slackapp

import "github.com/slack-go/slack"

const (
	// Slack limits
	SlackMaxDialogSelectOptions = 100
	// The slack library we are using is missing this type definition
	SlackInteractionTypeGlobalShortcut = slack.InteractionType("shortcut")

	// Supported Slack app feature types
	SlackFeatureInteraction  = "interaction"
	SlackFeatureSlashCommand = "slash-command"
	SlackFeatureEvent        = "event"
)
