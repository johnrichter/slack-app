# Slack App Manager

A service which handles the management of a Slack app and interactions with the Slack API on its behalf.

## Purpose

This library serves as a pluggable template for building Slack Applications. It handles all callbacks from Slack
to Slack's API guidelines.

An example of a plugin for this library which adds functionality is the
[House Points Manager](https://gitlab.com/johnrichter/house-points-manager).

# Development

Given the polyrepo setup of the various components, you may need to specify local locations for one or more
dependencies. Use the `go.mod.local` file as a guide to build and test this repo locally

In addition, see the [contribution guidelines](CONTRIBUTING.md).
