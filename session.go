package slackapp

import (
	"encoding/json"
	"net/http"
	"regexp"

	"github.com/rs/zerolog/log"
	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
	"gitlab.com/johnrichter/tracing-go"
)

var slackBotUserAgentMatcher = regexp.MustCompile(
	`^Slack(?:bot 1\.0|bot-LinkExpanding 1\.0|-ImgProxy 0\.19) ?\(\+https:\/\/api\.slack\.com\/robots\)$`,
)

func IsSlackbotUserAgent(ua string) bool {
	return slackBotUserAgentMatcher.MatchString(ua)
}

type SlackSession struct {
	Api               *slack.Client
	clientID          string
	clientSecret      string
	signingSecret     string
	verificationToken string
	botToken          string
}

func NewSlackSession(config *SlackConfig) *SlackSession {
	c := SlackSession{
		// We've wrapped the default client for tracing purposes and need to use it here
		Api:               slack.New(config.SlackBotToken, slack.OptionHTTPClient(tracing.SlackHttpClient)),
		clientID:          config.SlackClientID,
		clientSecret:      config.SlackClientSecret,
		signingSecret:     config.SlackSigningSecret,
		verificationToken: config.SlackVerificationToken,
		botToken:          config.SlackBotToken,
	}
	return &c
}

// IsSlackCallbackValid verifies that the request from slack has been signed correctly using our shared secrets
func (s *SlackSession) IsCallbackValid(r *http.Request) bool {
	body := CopyRequestBody(r)

	sv, err := slack.NewSecretsVerifier(r.Header, s.signingSecret)
	if err != nil {
		log.Debug().Msg("Slack callback missing secrets verification data")
		return false
	}
	sv.Write(body)
	if err = sv.Ensure(); err != nil {
		log.Debug().Msg("Slack callback failed secrets verification")
		return false
	}
	return true
}

// IsSlashCommandCallbackValid verifies that the command from Slack is authentic by using our shared verification token
func (s *SlackSession) GetSlashCommand(r *http.Request) (*slack.SlashCommand, bool) {
	sc, err := slack.SlashCommandParse(r)
	if err != nil {
		log.Debug().Msg("Unable to generate Slack Slash Command from request")
		return nil, false
	} else if !sc.ValidateToken(s.verificationToken) {
		log.Debug().Msg("Slack Slash Command failed token validation")
		return nil, false
	}
	return &sc, true
}

// IsInteractivityCallbackValid verifes that the interaction payload is not malformed
func (s *SlackSession) GetInteractivityCallback(r *http.Request) (*slack.InteractionCallback, bool) {
	if err := r.ParseForm(); err != nil {
		log.Debug().Msg("Unable to parse Slack Interactivity callback")
		return nil, false
	}
	payload := r.Form.Get("payload")
	if payload == "" {
		log.Debug().Msg("Missing interaction data in Slack Interactivity callback")
		return nil, false
	}
	var cb slack.InteractionCallback
	if err := json.Unmarshal([]byte(payload), &cb); err != nil {
		log.Debug().Msg("Unable to generate Slack Interaction from request")
		return nil, false
	}
	return &cb, true
}

func (s *SlackSession) GetEvent(r *http.Request) (*slackevents.EventsAPIEvent, bool) {
	event, err := slackevents.ParseEvent(
		CopyRequestBody(r),
		slackevents.OptionVerifyToken(
			&slackevents.TokenComparator{VerificationToken: s.verificationToken},
		),
	)
	if err != nil {
		log.Debug().Err(err).Msg("Unable to generate Slack Event from request")
		return nil, false
	}
	return &event, true
}

// func (s *SlackSession) HashWithHMAC(data []byte) hash.Hash {
// 	h := hmac.New(sha256.New, []byte(s.signingSecret))
// 	h.Write(data)
// 	return h
// }

// func (s *SlackSession) ValidateSlackRequestSignature(requestSignature, requestTimestamp, requestBody string) bool {
// 	sigBase := fmt.Sprintf("v0:%s:%s", requestTimestamp, requestBody)
// 	sigHash := s.HashWithHMAC([]byte(sigBase))
// 	sigHex := fmt.Sprintf("v0=%s", hex.EncodeToString(sigHash.Sum(nil)))
// 	return requestSignature == sigHex
// }
